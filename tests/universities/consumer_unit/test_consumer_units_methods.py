import pytest
from datetime import date

from tests.test_utils import dicts_test_utils
from tests.test_utils import create_objects_test_utils
from universities.models import ConsumerUnit
from contracts.models import EnergyBill, Contract
from tariffs.models import Distributor


@pytest.mark.django_db
class TestConsumerUnitMethods:
    def setup_method(self):
        self.university_dict = dicts_test_utils.university_dict_1

        self.university = create_objects_test_utils.create_test_university(self.university_dict)

        self.consumer_unit1 = ConsumerUnit.objects.create(
        name='Darcy Ribeiro',
        code='123456789',
        university=self.university
        )
        self.consumer_unit2 = ConsumerUnit.objects.create(
        name='Faculdade do Gama',
        code='123456780',
        university=self.university
        )

        self.distributor = Distributor.objects.create(
            name='Your Distributor Name',
            cnpj='12345678901234', 
            university=self.university,
            is_active=True,  
        )

        self.contract1 = Contract.objects.create(
            start_date=date.today(),
            tariff_flag='G', 
            supply_voltage=33.0,  
            peak_contracted_demand_in_kw=100.0,  
            off_peak_contracted_demand_in_kw=50.0,  
            distributor=self.distributor,
            consumer_unit=self.consumer_unit1,
        )

        self.contract2 = Contract.objects.create(
            start_date=date.today(),
            tariff_flag='G', 
            supply_voltage=33.0,  
            peak_contracted_demand_in_kw=100.0,  
            off_peak_contracted_demand_in_kw=50.0,  
            distributor=self.distributor,
            consumer_unit=self.consumer_unit2,
        )

        self.energy_bill1 = EnergyBill.objects.create(
            contract=self.contract1,
            consumer_unit=self.consumer_unit1,
            date=date.today(),
            invoice_in_reais=100.00,  
            is_atypical=False,  
            peak_consumption_in_kwh=50.0,  
            off_peak_consumption_in_kwh=30.0,
            peak_measured_demand_in_kw=10.0, 
            off_peak_measured_demand_in_kw=5.0, 
        )
        

    def test_get_energy_bills_by_year(self):
        test_year = date.today().year

        result = self.consumer_unit1.get_energy_bills_by_year(test_year)

        assert isinstance(result, list)

        for energy_bill_info in result:
            assert 'month' in energy_bill_info
            assert 'year' in energy_bill_info
            assert 'energy_bill' in energy_bill_info

    

    def test_get_energy_bills_by_year_with_future_year(self):
        test_year = date.today().year + 1

        with pytest.raises(Exception) as error:
            self.consumer_unit1.get_energy_bills_by_year(test_year)
        assert str(error.value) == 'Consumer User do not have Energy Bills this year'

    def test_get_energy_bills_by_year_from_before_consumer_unity_creation(self):
        test_year = date.today().year - 1

        with pytest.raises(Exception) as error:
            self.consumer_unit1.get_energy_bills_by_year(test_year)
        assert str(error.value) == 'Consumer User do not have Energy Bills this year'


    def test_get_energy_bills_by_year_with_no_bills(self):
        test_year = date.today().year

        result = self.consumer_unit2.get_energy_bills_by_year(test_year)

        assert isinstance(result, list)

        for energy_bill_info in result:
            assert 'month' in energy_bill_info
            assert 'year' in energy_bill_info
            assert energy_bill_info['energy_bill'] == None